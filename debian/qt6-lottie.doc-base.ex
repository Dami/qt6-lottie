Document: qt6-lottie
Title: Debian qt6-lottie Manual
Author: <insert document author here>
Abstract: This manual describes what qt6-lottie is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/qt6-lottie/qt6-lottie.sgml.gz

Format: postscript
Files: /usr/share/doc/qt6-lottie/qt6-lottie.ps.gz

Format: text
Files: /usr/share/doc/qt6-lottie/qt6-lottie.text.gz

Format: HTML
Index: /usr/share/doc/qt6-lottie/html/index.html
Files: /usr/share/doc/qt6-lottie/html/*.html
